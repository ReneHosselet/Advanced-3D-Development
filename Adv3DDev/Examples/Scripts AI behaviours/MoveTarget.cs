﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTarget : MonoBehaviour
{
    public float rotationSpeed = 1;
    public float frequency = 0.1f; //w= 2 pi f

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //rotatie rond punt (vector3.zero)
        transform.RotateAround(Vector3.zero, Vector3.up, rotationSpeed * Time.deltaTime * Mathf.Sin(Time.timeSinceLevelLoad * frequency * 2 * Mathf.PI));
    }
}
