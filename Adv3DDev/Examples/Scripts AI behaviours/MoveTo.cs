﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MoveTo : MonoBehaviour
{
    public Transform goal;
    public float speed;
    public float arriveRadius;
    public float shadowRadius;
    public float fleeRadius;
    public float turningSpeed;
    public float wanderDist;
    public float wanderSpeed;
    public float wanderRad;
    public float minTime;
    public float maxTime;
    public SteeringBehaviours behaviour;
    public float obstacleAvoidanceForce;
    public float obstacleAvoidanceDistance;
    public float obstacleAvoidanceSpread;
    public bool obstacleAvoid;
    public float separationDistance;
    public float separationForce;
    public bool sep;
    public float cohesionDistance;
    public float cohesionForce;
    public bool coh;

    float timer;
    float wanderTimer = 1;

    public List<Vector3> ObstacleAvoidanceRays;
   
    NavMeshAgent agent;
    Vector3 wanderDirec;
    Vector3 steerForce;
    Rigidbody rb;

    private NavMeshPath newMeshPath ;
    public float elapsedFrames;

    void Start()
    {
        agent = GetComponent <NavMeshAgent>();
        rb = GetComponent<Rigidbody>();
        behaviour = SteeringBehaviours.Seek;
        goal = GameObject.FindGameObjectWithTag("player").transform;
        
    }
    
    void Update()
    {
       steerForce =  Vector3.zero;

        switch (behaviour)
        {
            case SteeringBehaviours.Seek:
                steerForce += Seek(goal.transform.position);
                break;
            case SteeringBehaviours.Flee:
                steerForce += Flee(goal.transform.position);
                break;
            case SteeringBehaviours.Arrive:
                steerForce += Arrive(goal.transform.position);
                break;
            case SteeringBehaviours.Wander:
                timer += Time.deltaTime;
                if (timer > wanderTimer)
                {
                    timer = 0;
                    wanderTimer = Random.Range(minTime, maxTime);
                    wanderDirec = Wander();
                }
                steerForce += Seek(wanderDirec) * wanderSpeed;
                break;
            case SteeringBehaviours.Shadow:
                steerForce += Shadow(goal.transform.position);
                break;
            case SteeringBehaviours.FollowPath:
                steerForce += FollowPath(goal.transform.position);
                break;
            default:
                break;
        }
        if (obstacleAvoid)
        {
            steerForce += ObstacleAvoidance();
        }
        if (sep)
        {
            steerForce += Separation();
        }
        if (coh)
        {
            steerForce += Cohesion();
        }
        
        rb.AddForce(steerForce * speed/*, ForceMode.Force*/);
        //object laten draaien naar zijn richting
        Vector3 turningDirection = Vector3.RotateTowards(transform.forward, (goal.transform.position - transform.position) + 1.25f * rb.velocity, turningSpeed * Time.deltaTime, 0.0f);
        transform.rotation = Quaternion.LookRotation(turningDirection);
        
        if (Input.GetKeyDown(KeyCode.F))
        {
            behaviour = SteeringBehaviours.Flee;
        }
        else if(Input.GetKeyDown(KeyCode.S))
        {
            behaviour = SteeringBehaviours.Seek;
        }
        else if (Input.GetKeyDown(KeyCode.A))
        {
            behaviour = SteeringBehaviours.Arrive;
        }
        else if (Input.GetKeyDown(KeyCode.W))
        {
            behaviour = SteeringBehaviours.Wander;
        }
        else if (Input.GetKeyDown(KeyCode.D))
        {
            behaviour = SteeringBehaviours.Shadow;
        }
        else if (Input.GetKeyDown(KeyCode.P))
        {
            behaviour = SteeringBehaviours.FollowPath;
        }
        
    }
    public Vector3 Seek(Vector3 sp)
    {
        Debug.DrawLine(transform.position, sp, Color.red);
        return (sp - transform.position).normalized * speed;
    }
    public Vector3 Flee(Vector3 sp)
    {

        Debug.DrawLine(transform.position, sp, Color.red);
        return (transform.position - sp).normalized * speed; ;
    }
    public Vector3 Arrive(Vector3 sp)
    {
        Vector3 forceToReturn = Vector3.zero;
        Debug.DrawLine(transform.position, sp, Color.red);
        if (Vector3.Distance(sp,transform.position)> arriveRadius)
        {
            forceToReturn = Seek(sp);
        }
        return forceToReturn;
    }
    public Vector3 FollowPath(Vector3 sp)
    {
        Vector3 forceToReturn = Vector3.zero;
        newMeshPath = new NavMeshPath();
        if (Time.frameCount % elapsedFrames == 0)
        {
            NavMesh.CalculatePath(transform.position, sp, NavMesh.AllAreas, newMeshPath);
        }
        for (int i = 0; i < newMeshPath.corners.Length - 1; i++)
        {
            Debug.DrawLine(newMeshPath.corners[i], newMeshPath.corners[i + 1], Color.magenta);
            forceToReturn = newMeshPath.corners[i];
        }
        return forceToReturn;
        //agent.destination = goal.position;  //let agent follow target using mesh agent
    }
    public Vector3 Shadow(Vector3 positionToArrive)
    {
        Vector3 forceToReturn = Vector3.zero;
        if (Vector3.Distance(positionToArrive, transform.position) > arriveRadius)
        {
            forceToReturn = Seek(positionToArrive);
        }
        else if (Vector3.Distance(positionToArrive, transform.position) < shadowRadius)
        {
            forceToReturn = Flee(positionToArrive);
        }
        else
        {
            // do nothing
        }
        return forceToReturn;
    }
    public Vector3 Wander()
    {
        Vector3 posToRet = transform.position + transform.forward * wanderDist + Random.onUnitSphere * wanderRad;
        posToRet.y = transform.position.y;
        Debug.DrawLine(transform.position, posToRet, Color.red);
        return posToRet;
    }
    public Vector3 ObstacleAvoidance()
    {
        Vector3 forceToReturn = Vector3.zero;
        RaycastHit hit;
        for (int i = 0; i < ObstacleAvoidanceRays.Count; i++)
        {
            if (Physics.Raycast(transform.position,
                transform.forward * ObstacleAvoidanceRays[i].z + transform.up * ObstacleAvoidanceRays[i].y + transform.right * ObstacleAvoidanceRays[i].x,
                out hit, ObstacleAvoidanceRays[i].magnitude * obstacleAvoidanceDistance))
            {
                float dist = Vector3.Distance(transform.position, hit.point);
                forceToReturn += obstacleAvoidanceForce
                                * (transform.position - hit.point)
                                * ((ObstacleAvoidanceRays[i].magnitude * obstacleAvoidanceDistance) - dist) / ((ObstacleAvoidanceRays[i].magnitude * obstacleAvoidanceDistance) + dist);
                //https://www.google.com/search?sxsrf=ACYBGNR_9VA-KkfpybWJMeDbdx6NEvQFfA%3A1570616457714&ei=ibSdXcWeK821sAem7oCwBQ&q=f%28x%29+%3D+%28%282-x%29%2F%282%2Bx%29%29&oq=f%28x%29+%3D+%28%282-x%29%2F%282%2Bx%29%29&gs_l=psy-ab.3...0.0..2607...0.0..0.0.0.......0......gws-wiz.6XtROx2swEk&ved=0ahUKEwiFh6mg-o7lAhXNGuwKHSY3AFYQ4dUDCAs&uact=5
                Debug.DrawLine(transform.position, hit.point, Color.black);
            }
        }
        return forceToReturn;
    }
    public Vector3 Separation()
        {
        Vector3 forceToReturn = Vector3.zero;
        GameObject[] allDrones = GameObject.FindGameObjectsWithTag("Drone");
        for (int i = 0; i < allDrones.Length; i++)
        {
            float dist = Vector3.Distance(transform.position,allDrones[i].transform.position);
            if (dist < separationDistance)
            {
                forceToReturn += (transform.position - allDrones[i].transform.position).normalized*separationForce*(separationDistance-dist)/(separationDistance + dist);
            }
        }
        Debug.DrawLine(transform.position, transform.position+forceToReturn,Color.green);
        return forceToReturn;
    }
    public Vector3 Cohesion()
    {
        Vector3 forceToReturn = Vector3.zero;
        GameObject[] allDrones = GameObject.FindGameObjectsWithTag("Drone");
        Vector3 center = Vector3.zero;
        int nrOfDronesCloseEnough = 0;
        for (int i = 0; i < allDrones.Length; i++)
        {
            if (Vector3.Distance(transform.position, allDrones[i].transform.position) < cohesionDistance)
            {
                center += allDrones[i].transform.position;
                nrOfDronesCloseEnough++;
            }
        }
        center /= nrOfDronesCloseEnough;
        Debug.DrawLine(transform.position, center, Color.blue);
        forceToReturn = Seek(center) * cohesionForce;
        return forceToReturn;
    }
    
    
    public enum SteeringBehaviours
    {
        Seek = 1,
        Flee,
        Arrive,
        Wander,
        Shadow,
        FollowPath,
    }
}
