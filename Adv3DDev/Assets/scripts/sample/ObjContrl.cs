﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

public class ObjContrl : MonoBehaviour
{
    public SerialController serialController;
    public string[] ypr;
    public string[] aWorld;
    public Material skyMat1;
    public Material skyMat2;
    //yaw pitch roll
    int i = 0;
    public float y;
    public float p;
    public float r;
    //aworld accelerometer
    int j = 0;
    public float xWorld;
    public float yWorld;
    public float zWorld;
    // Start is called before the first frame update
    void Start()
    {
    }
    // Update is called once per frame
    void Update()
    {
        //---------------------------------------------------------------------
        // Send data
        //---------------------------------------------------------------------

        // If you press one of these keys send it to the serial device. A
        // sample serial device that accepts this input is given in the README.
        if (Input.GetKeyDown(KeyCode.A))
        {
            Debug.Log("Sending A");
            serialController.SendSerialMessage("A");
        }
        if (Input.GetKeyDown(KeyCode.Z))
        {
            Debug.Log("Sending Z");
            serialController.SendSerialMessage("Z");
        }
        //---------------------------------------------------------------------
        // Receive data
        //---------------------------------------------------------------------
        string message = serialController.ReadSerialMessage();

        if (message == null)
            return;

        // Check if the message is plain data or a connect/disconnect event.
        if (ReferenceEquals(message, SerialController.SERIAL_DEVICE_CONNECTED))
            Debug.Log("Connection established");
        else if (ReferenceEquals(message, SerialController.SERIAL_DEVICE_DISCONNECTED))
            Debug.Log("Connection attempt failed or disconnection detected");
        else if (message.Substring(0, 3) == "ypr")
        {
            ypr = message.Split('\t');
            Debug.Log(message);
            for (int i = 0; i < ypr.Length; i++)
            {
                if (ypr[i] == "ypr") { 
                        y = float.Parse(ypr[i+1], CultureInfo.InvariantCulture);
                        p = float.Parse(ypr[i+2], CultureInfo.InvariantCulture);
                        r = float.Parse(ypr[i+3], CultureInfo.InvariantCulture);
                }  
                else if (ypr[i] == "ls"){
                    int lum = int.Parse(ypr[i+1]);            
                    ChangeSky(lum);
                    Debug.Log(lum + "/"+ message);
                    Debug.Log(RenderSettings.skybox);
                }                
            } 
            gameObject.transform.rotation = Quaternion.Euler(y,p,r);
        }              
    }
    public bool night = false;
    public void ChangeSky(int lumen){
        if (lumen < 250)
        {
            if (!night)
            {
            Debug.Log("dark");
            RenderSettings.skybox=skyMat2; 
            night = true;
            }            
        }
        else {
            if (night)
            {
            Debug.Log("day");
            RenderSettings.skybox=skyMat1; 
            night = false;
            }            
        }            
        }
}
// TODO 
// serial output optimiser -faster change of sky
//arduino serial write in 1 line (println) for optimalisation